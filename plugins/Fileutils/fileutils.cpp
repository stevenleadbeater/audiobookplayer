#include <QDebug>
#include <QtQml>
#include <QtQml/QQmlContext>
#include "fileutils.h"
#include "plugin.h"
#include <MediaHub/Player>

Fileutils::Fileutils() {

}

void Fileutils::speak() {
    qDebug() << "hello world!";
}

void Fileutils::sleepAfter(double seconds) {
    //qDebug() << Q_FUNC_INFO;
// Only one player session needed
    if (m_hubPlayerSession == nullptr) {

        try {
            qDebug() << "Starting a new media-hub player session...";
            m_hubPlayerSession = std::make_shared<lomiri::MediaHub::Player>(true);
            qDebug() << "got media hub session!";
        }
        catch (const std::runtime_error &e) {
            qWarning() << "Failed to start a new media-hub player session: " << e.what();
            return;
        }


        // Get the player session UUID, so we can suspend/restore our session when the ApplicationState
        // changes
        m_sessionUuid = m_hubPlayerSession->uuid();
        qDebug() << "media hub UUID " << m_sessionUuid;
        /* << lomiri::MediaHub::Player::canPlay();*/
        //Q_EMIT lomiri::MediaHub::Player::volumeChanged();
    }
    m_hubPlayerSession->setSleepAfter(seconds * 1000);
    qDebug() << "media hub call " << m_hubPlayerSession->sleepAfter();
    //m_hubPlayerSession->setSleepAfter(seconds);
}

double Fileutils::remainingTimeUntilSleep() {
// Only one player session needed
    if (m_hubPlayerSession == nullptr) {

        try {
            qDebug() << "Starting a new media-hub player session...";
            m_hubPlayerSession = std::make_shared<lomiri::MediaHub::Player>(true);
            qDebug() << "got media hub session!";
        }
        catch (const std::runtime_error &e) {
            qWarning() << "Failed to start a new media-hub player session: " << e.what();
            return 0;
        }


        // Get the player session UUID, so we can suspend/restore our session when the ApplicationState
        // changes
        m_sessionUuid = m_hubPlayerSession->uuid();
        qDebug() << "media hub UUID " << m_sessionUuid;
        /* << lomiri::MediaHub::Player::canPlay();*/
        //Q_EMIT lomiri::MediaHub::Player::volumeChanged();
    }
    double remainingTime = m_hubPlayerSession->remainingTimeUntilSleep();
    return remainingTime;
    //m_hubPlayerSession->setSleepAfter(seconds);
}

void Fileutils::moveFile(QString source, QString destination, QString fileName)
{
    //qDebug() << Q_FUNC_INFO << "Copying moveImage from" << source << "to" << destination;

    QDir dir(destination);
    if (!dir.exists())
        dir.mkpath(".");

    const QString srcFilePath = source + QLatin1Char('/') + fileName;
    const QString destFilePath = destination + QLatin1Char('/') + fileName;

    QFile::copy(srcFilePath, destFilePath);
}

/*
  Delete an image
*/
void Fileutils::removeFile(QString source)
{
    if(source.startsWith("file://"))
      source.remove("file://");

    QFile file(source);
    file.remove();
}

/*
   The returned value, depends on the target device :
   '/home/phablet' when running on device
   '/tmp' when on Desktop. NOTE: '/tmp' is a folder inside clickable container, NOT host machine
 */
QString Fileutils::getHomePath()
{
    return QDir::homePath();
}

/*
  Get the list of images owned
*/
QStringList Fileutils::getFiles(QString path)
{
    //qDebug() << "Path" << path;
    QStringList list;

    if(path.isEmpty())
      return list;

    if(path.startsWith("file://"))
       path.remove("file://");

    /*QDir dir(path);
    list = dir.entryList(QDir::Files);
    //qDebug() << "Files" << list;*/
    QDirIterator it(path, QDir::Files, QDirIterator::Subdirectories);
    while(it.hasNext()) {
        it.next();
        list << it.fileName();
    }

    list.sort(Qt::CaseInsensitive);

    return list;
}

/*
  Remove images
*/
void Fileutils::deleteMomentFolder(QString path)
{
    QStringList list;

    if(path.startsWith("file://"))
       path.remove("file://");

    QDir dir(path);
    dir.removeRecursively();
    //list = dir.entryList(QDir::Files);
}
