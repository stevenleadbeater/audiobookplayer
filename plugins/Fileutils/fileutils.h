#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QRegularExpression>
#include <QDir>
#include <QDateTime>
#include <MediaHub/Player>
#include <memory>

class Fileutils: public QObject {
    Q_OBJECT

    std::shared_ptr<lomiri::MediaHub::Player> m_hubPlayerSession;
    QString m_sessionUuid;

public:
    Fileutils();
   ~Fileutils() = default;
   /* IMPORTANT: Without Q_INVOKABLE as prefix, the method is NOT exposed to QML (ie: is non invokable) causing error like:
      TypeError: Property <metho-name> of object <pluginName>(0x284d8d0) is not a function
   */
   Q_INVOKABLE void moveFile(QString source, QString destination, QString fileName);
   Q_INVOKABLE void removeFile(QString source);
   Q_INVOKABLE QStringList getFiles(QString path);
   Q_INVOKABLE void deleteMomentFolder(QString path);
   Q_INVOKABLE QString getHomePath();
   Q_INVOKABLE void speak();
   Q_INVOKABLE void sleepAfter(double seconds);
   Q_INVOKABLE double remainingTimeUntilSleep();
};

#endif
