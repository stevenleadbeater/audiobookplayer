import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Pickers 1.3
import Lomiri.Layouts 1.0
import Lomiri.Content 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import QtQuick.LocalStorage 2.7
import Qt.labs.platform 1.0

import Fileutils 1.0

import "js/db.js" as DB
import "js/files.js" as Files
import "./dialog"

MainView {

    id: root

    objectName: "mainView"
    automaticOrientation: true
    anchorToKeyboard: true

    applicationName: "audiobookplayer.ledsys"

    property var activeTransfer: null

    signal importRequested
    signal bookSelected(string bookPath)

    width: units.gu(100)
    height: units.gu(75)

    /* list of imported books */
    ListModel{
       id: importedBooksListModel
    }

    onBookSelected: {
        console.log("1. Book selected: " + bookPath)
        var progress = DB.getProgress(bookPath);
        DB.setLastOpen(bookPath);
        const coverArtPath = Files.getCoverArt(bookPath);
        pageStack.clear();
        pageStack.push(Qt.resolvedUrl("BookPage.qml"), {bookPath: bookPath, progress: progress, coverArtPath: coverArtPath});
    }

    /* ------------- Mandatory blocks: Connection with the ContentHub component ---------------
    Note: must is placed in the MainView Component of the App, not in others Pages Component */
    Connections {
         target: ContentHub
         /* handler for event 'importRequested' raised when user press 'Browse' button or 'Add' button to choose a book to import */
         onImportRequested: {
                console.log("ImportRequested event raised: import book process started...");
                /* bind the 'transfer' property of the ContentHub, to the local var 'activeTransfer' (see above) */
                root.activeTransfer = transfer

                if (root.activeTransfer.state === ContentTransfer.Charged){
                   /* book transfer completed from chosen Pictures App sources */
                   Files.processBooks(root.activeTransfer.items)
                }
         }
    }

    /* Connection with the 'activeTransfer' local variable which is bound with the 'transfer' object of the
       ContentHub (see above).
       This variable in monitored to get transfer status
    */
    Connections {
        target: root.activeTransfer
        onStateChanged: {
           console.log("Transfer State Changed to status: " + root.activeTransfer.state);
           if (root.activeTransfer.state === ContentTransfer.Charged){
               /* transfer is completed from the chosen Pictures App source */
               Files.processBooks(root.activeTransfer.items)
           }
       }
    }
    //------------------------------------------------------------------------------------


    PageStack {
        id: pageStack

        anchors.fill: parent

        /* load the first page */
        Component.onCompleted: {
            DB.initializeDB()
            Files.processMissedTransfers();
            const lastOpen = DB.getLastOpen();
            if (lastOpen !== "") {
                const progress = DB.getProgress(lastOpen);
                const coverArtPath = Files.getCoverArt(lastOpen);
                pageStack.push(Qt.resolvedUrl("BookPage.qml"), {bookPath: lastOpen, progress: progress, coverArtPath: coverArtPath})
            } else {
                pageStack.push(Qt.resolvedUrl("LibraryPage.qml"))
            }
        }
    }

}
