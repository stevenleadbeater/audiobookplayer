import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Pickers 1.3
import Lomiri.Content 1.3
import Lomiri.Layouts 1.0
import Lomiri.Components.ListItems 1.3 as ListItem

/* import folder */
import "./dialog"

/* custom plugin */
import Fileutils 1.0
import "js/files.js" as Files


/*
  Page that show a list of imported books using a ListItem Component.
  With swipe movements on right/left on a item allow to:
  - delete the book
*/
Page {
    id: libraryPage

    /* the currently selected book name in the ListItem */
    property string targetBookName;

    header: PageHeader {
        title: i18n.tr("Audiobooks found") + ": " + importedBooksListModel.count
        trailingActionBar.actions: [
            Action {
                iconName: "add"
                text: i18n.tr("Add")
                onTriggered: {
                    /* Emit a signal named 'importRequested' to notify that we want import a book.
                           The signal handler is in Main.qml  file: it connects with Contenthub to start a transfer.
                        */
                    root.importRequested()

                    /* show a page containing the App in the device that are registered as "Book source" */
                    pageStack.push(Qt.resolvedUrl("ContentPeerPickerPage.qml"))
                }
            }
        ]
    }

    Connections {
        target: root.activeTransfer
        onStateChanged: {
            console.log("LibraryPage: Transfer State Changed to status: " + root.activeTransfer.state + "waiting for state: " + ContentTransfer.Collected);
            if (root.activeTransfer.state === ContentTransfer.Collected) {
                console.log("LibraryPage: start timer to refresh book list");
                loadTimer.running = true;
            }
        }
    }

    Timer {
        id: loadTimer
        interval: 200;
        running: false;
        repeat: false
        onTriggered: {
            console.log("LibraryPage: refreshing book list");
            getBookList();
        }
    }

    Component.onCompleted: {
        /* load the already imported books */
        Files.processMissedTransfers();
        getBookList();
    }

    /*
      Show the books imported
    */
    LomiriListView {
        id: bookListView
        anchors.fill: parent
        anchors.topMargin: units.gu(6)
        model: importedBooksListModel
        /* delegate component used to draw an item (ie an book item) in the LomiriListView */
        delegate: BookNamesListDelegate {
        }
    }

    /*
      Load from the App folder (/home/phablet/.local/share/<project_name>) the imported book names
    */
    function getBookList() {
        var bookPath = Fileutils.getHomePath() + "/" + Files.getBooksSavingRootPath() + "/audiobooks";
        console.log("Importing audiobooks from folder: " + bookPath);
        var fileList = Fileutils.getFiles(bookPath);

        importedBooksListModel.clear();

        /* fill the ListModel with the books found */
        for (var i = 0; i < fileList.length; i++) {
            console.log("Found audiobook name: " + fileList[i])
            importedBooksListModel.append({
                bookName: fileList[i],
                bookPath: bookPath,
                value: i,
                coverArtPath: Files.getCoverArt(fileList[i])
            });
        }
    }
}
