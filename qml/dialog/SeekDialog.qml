import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "../js/date.js" as Date
/*
   Show the provided image in a dedicated Dialog
*/
Popover {
    id: seekDialog
    property string currentPosition;
    Column {
        id: containerLayout
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }

        ListItem {
            // shall specify the height when Using ListItemLayout inside ListItem
            height: seekFieldHours.height + (divider.visible ? divider.height : 0)
            TextField {
                id: seekFieldHours
                text: Date.getHours(currentPosition)
                anchors.left: parent.left
                width: parent.width / 3
            }
            TextField {
                id: seekFieldMinutes
                text: Date.getMinutes(currentPosition)
                anchors.left: seekFieldHours.right
                width: parent.width / 3
            }
            TextField {
                id: seekFieldSeconds
                text: Date.getSeconds(currentPosition)
                anchors.left: seekFieldMinutes.right
                width: parent.width / 3
            }
        }
        ListItem {
            // shall specify the height when Using ListItemLayout inside ListItem
            height: seekButton.height + (divider.visible ? divider.height : 0)
            Button {
                id: seekButton
                text: "Seek"
                anchors.left: seekField.right
                onClicked: {
                    const seekTo = (parseInt(seekFieldHours.text) * 3600 + parseInt(seekFieldMinutes.text) * 60 + parseInt(seekFieldSeconds.text)) * 1000
                    bookPage.setProgressSignal(seekTo)
                    PopupUtils.close(seekDialog)
                }
            }
        }
    }
}
