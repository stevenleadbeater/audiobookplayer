import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import Fileutils 1.0

/*
   Show the provided image in a dedicated Dialog
*/
Popover {
    id: sleepDialog
    property int currentIndex: 0

    Column {
        id: containerLayout
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }

        ListItem.ItemSelector {
            id: sleepAfterSelector
            text: i18n.tr("Sleep after")
            expanded: true
            model: [i18n.tr("5 Minutes"), i18n.tr("10 Minutes"), i18n.tr("15 Minutes"), i18n.tr("30 Minutes"), i18n.tr("1 Hour")]
            onSelectedIndexChanged: {
                console.log("Selected index: " + selectedIndex)
                currentIndex = selectedIndex
            }
        }

        Button {
            id: sleepAfterButton
            text: "OK"
            anchors.left: parent.left
            onClicked: {
                console.log("Sleep after: " + currentIndex);
                let sleepTime = 0;
                switch (currentIndex) {
                    case 0:
                        sleepTime = 300;
                        break;
                    case 1:
                        sleepTime = 600;
                        break;
                    case 2:
                        sleepTime = 900;
                        break;
                    case 3:
                        sleepTime = 1800;
                        break;
                    case 4:
                        sleepTime = 3600;
                        break;
                }
                Fileutils.sleepAfter(sleepTime);
                bookPage.setRemainingTimeUntilSleepSignal(sleepTime);
                PopupUtils.close(sleepDialog)
            }
        }
    }

}
