import QtQuick 2.4
import QtQuick.Controls 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Components.Pickers 1.3
import Lomiri.Layouts 1.0
import Lomiri.Content 1.3
import Lomiri.Components.ListItems 1.3 as ListItem
import QtMultimedia 5.6
import Qt.labs.platform 1.0

/*
  Custom plugin to perform some operations with files on the device
*/
import Fileutils 1.0
import Lomiri.Thumbnailer 0.1

import "js/db.js" as DB
import "js/date.js" as Date
import "./dialog"

Page {
    id: bookPage
    property string bookPath;
    property string coverArtPath;
    property int progress;
    property bool isPlaying: false;

    signal setProgressSignal(int progressSignal)

    onSetProgressSignal: {
        console.log("set progress signal: " + progressSignal)
        player.seek(progressSignal);
        DB.saveProgress(progressSignal)
        progress = progressSignal
    }

    signal setRemainingTimeUntilSleepSignal(int remainingTimeUntilSleepSignal)

    onSetRemainingTimeUntilSleepSignal: {
        remainingTimeUntilSleepTimer.running = true;
        remainingTimeUntilSleepLabel.visible = true;
        remainingTimeUntilSleepLabel.text = Date.msToMS(remainingTimeUntilSleepSignal);
    }

    header: PageHeader {
        title: "<b>" + bookPath + "</b>"
        trailingActionBar.actions: [
            Action {
                iconName: "media-playlist"
                text: i18n.tr("Library")
                onTriggered: {
                    pageStack.push(Qt.resolvedUrl("LibraryPage.qml"))
                }
            }
        ]
    }

    Connections {
        target: Qt.application

        onAboutToQuit: {
            player.stop()
        }
    }

    Component {
        id: seekDialog
        SeekDialog {
            currentPosition: sliderProgress.value
        }
    }

    Component {
        id: sleepDialog
        SleepDialog {
        }
    }

    MediaPlayer {
        id: player
        onPositionChanged: {
            sliderProgress.value = position
            if (position > progress) {
                progress = position
            }
            DB.saveProgress(position)
            progressLabel.text = Date.msToHMS(position)
            sliderProgress.value = position
            //console.log(Date.msToHMS(position))
        }
        onDurationChanged: {
            durationLabel.text = Date.msToHMS(player.duration)
            sliderProgress.maximumValue = duration
        }
        onSourceChanged: {
            console.log("source changed 2")
            sliderProgress.minimumValue = 0
            sliderProgress.value = progress
            progressLabel.text = Date.msToHMS(progress)
            console.log(player.source)
            nowPlayingLabel.text = bookPath
            loadTimer.running = true;
        }
        onPlaybackStateChanged: {
            console.log("playback state changed to: " + player.playbackState)
            if (player.playbackState === 0 || player.playbackState === 2) {
                playButton.source = Qt.resolvedUrl("icons/Play_Icon.svg");
                isPlaying = false;
                remainingTimeUntilSleepTimer.running = false;
                remainingTimeUntilSleepLabel.visible = false;
            } else {
                playButton.source = Qt.resolvedUrl("icons/Pause_Icon.svg");
                isPlaying = true;
            }
        }
        source: StandardPaths.writableLocation(StandardPaths.AppDataLocation) + "/audiobooks/" + bookPath
    }

    Timer {
        id: loadTimer
        interval: 500;
        running: false;
        repeat: false
        onTriggered: {
            console.log("play")
            player.play();
            player.seek(progress);
            player.pause();
        }
    }

    Label {
        id: nowPlayingLabel
        text: i18n.tr('Hello World!')

        verticalAlignment: Label.AlignVCenter
        horizontalAlignment: Label.AlignHCenter
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
        }
    }

    Image {
        id: coverImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        asynchronous: true
        fillMode: Image.PreserveAspectFit
        sourceSize.width: width
        width: Math.max(parent.height, parent.width)
        source: coverArtPath
    }

    Label {
        id: remainingTimeUntilSleepLabel
        text: "00:00:00"
        textSize: Label.XLarge
        visible: false
        horizontalAlignment: Label.AlignHCenter
        anchors {
            top: nowPlayingLabel.bottom
            bottom: sliderProgress.top
            left: parent.left
            right: parent.right
        }
    }

    Timer {
        id: remainingTimeUntilSleepTimer
        interval: 500;
        running: false;
        repeat: true
        onTriggered: {
            const remainingTimeUntilSleep = Fileutils.remainingTimeUntilSleep();
            if (remainingTimeUntilSleep > 0 && remainingTimeUntilSleep < 5000) {
                remainingTimeUntilSleepTimer.running = false;
                remainingTimeUntilSleepLabel.visible = false;
            }
            remainingTimeUntilSleepLabel.text = Date.msToMS(remainingTimeUntilSleep);
        }
    }

    Label {
        id: progressLabel
        text: "00:00:00"
        anchors {
            bottom: playButton.top
            bottomMargin: 30
            left: parent.left
            leftMargin: 30
        }
        MouseArea {
            anchors.fill: progressLabel
            onClicked: {
                console.log("clicked")
                player.pause();
                PopupUtils.open(seekDialog)
            }
        }
    }

    Label {
        id: durationLabel
        text: "00:00:00"
        anchors {
            bottom: playButton.top
            bottomMargin: 30
            right: parent.right
            rightMargin: 30
        }
    }

    Slider {
        id: sliderProgress
        live: true

        anchors {
            bottom: progressLabel.top
            left: parent.left
            right: parent.right
            leftMargin: 30
            rightMargin: 30
        }

        function formatValue(value) {
            return Date.msToHMS(value)
        }

        onTouched: {
            player.seek(sliderProgress.value);
            DB.saveProgress(sliderProgress.value)
            progress = sliderProgress.value
        }
    }

    Icon {
        id: backButton
        width: units.gu(6)
        height: units.gu(6)
        source: Qt.resolvedUrl("icons/Back.svg")

        MouseArea {
            anchors.fill: backButton
            onClicked: {
                setProgress(0);
            }
        }
        anchors {
            top: playButton.top
            topMargin: units.gu(1)
            left: parent.left
            leftMargin: 30
            bottomMargin: 30
        }
    }

    Icon {
        id: back60Button
        width: units.gu(6)
        height: units.gu(6)
        source: Qt.resolvedUrl("icons/Back_60.svg")

        MouseArea {
            anchors.fill: back60Button
            onClicked: {
                setProgress(progress - 60000);
            }
        }
        anchors {
            top: playButton.top
            topMargin: units.gu(1)
            left: backButton.right
            right: playButton.left
            bottomMargin: 30
        }
    }

    Icon {
        id: playButton
        width: units.gu(8)
        height: units.gu(8)
        source: Qt.resolvedUrl("icons/Play_Icon.svg")

        MouseArea {
            anchors.fill: playButton
            onClicked: {
                if (isPlaying) {
                    player.pause();
                    isPlaying = false;
                } else {
                    player.play();
                    player.seek(progress);
                    isPlaying = true;
                }
            }
        }

        anchors {
            bottom: sleepButton.top
            horizontalCenter: parent.horizontalCenter
            bottomMargin: 30
        }
    }

    Icon {
        id: forward60Button
        width: units.gu(6)
        height: units.gu(6)
        source: Qt.resolvedUrl("icons/Forward_60.svg")

        MouseArea {
            anchors.fill: forward60Button
            onClicked: {
                setProgress(progress + 60000);
            }
        }
        anchors {
            top: playButton.top
            topMargin: units.gu(1)
            left: playButton.right
            right: forwardButton.left
            bottomMargin: 30
        }
    }

    Icon {
        id: forwardButton
        width: units.gu(6)
        height: units.gu(6)
        source: Qt.resolvedUrl("icons/Forward.svg")

        MouseArea {
            anchors.fill: forwardButton
            onClicked: {
                setProgress(player.duration - 1);
            }
        }
        anchors {
            top: playButton.top
            topMargin: units.gu(1)
            right: parent.right
            rightMargin: 30
            bottomMargin: 30
        }
    }

    Icon {
        id: sleepButton
        width: units.gu(6)
        height: units.gu(6)
        source: Qt.resolvedUrl("icons/Sleep.svg")

        MouseArea {
            anchors.fill: sleepButton
            onClicked: {
                PopupUtils.open(sleepDialog)
                remainingTimeUntilSleepTimer.running = true;
            }
        }
        anchors {
            bottom: parent.bottom
            topMargin: units.gu(1)
            left: parent.left
            leftMargin: 30
            bottomMargin: 30
        }
    }

    function setProgress(value) {
        if (value < 0) {
            return;
        }
        player.seek(value);
        DB.saveProgress(value);
        progress = value;
    }
}
