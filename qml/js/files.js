function getBookNameFromPath(url){
    const bookName = url.split("/");
    return bookName[bookName.length -1];
}

function getPathFromUrl(url){
    return url.toString().replace("file://", "");
}

function getCoverArt(bookPath) {
    return "image://thumbnailer/" + StandardPaths.writableLocation(StandardPaths.AppDataLocation) + "/audiobooks/" + bookPath;
}

function getBooksSavingRootPath() {
    return ".local/share/audiobookplayer.ledsys";
}

function getContentHubImportPath() {
    return ".cache/audiobookplayer.ledsys/HubIncoming";
}

function processMissedTransfers() {
    var fileList = Fileutils.getFiles(getContentHubImportPath());
    processBooks(fileList);
}

function processBooks(items)
{
    /* An iteration is necessary if more books are imported */
    for (let i = 0; i < items.length; i++)
    {
        const url = items[i].url;
        const bookName = getBookNameFromPath(String(url));
        const sourcePath = getPathFromUrl(url).replace(bookName, "");
        const destinationPath = Fileutils.getHomePath() + "/" + getBooksSavingRootPath() + "/audiobooks";
        console.log("Moving book from path: " + sourcePath +" To destination path: " + destinationPath);
        Fileutils.moveFile(sourcePath, destinationPath,bookName);
        Fileutils.removeFile(sourcePath);
    }
}