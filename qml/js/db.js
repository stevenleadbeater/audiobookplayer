function getDatabase() {
    const dbName = "AudioBookPlayerDB"
    const dbVersion = "1.0"
    const dbDescription = "Database for audio book app"
    const dbEstimatedSize = 10000
    return LocalStorage.openDatabaseSync(dbName, dbVersion, dbDescription, dbEstimatedSize)
}

function getBookProgressTable() {
    return "BookProgress";
}

function getLastBookOpenTable() {
    return "LastBookOpen";
}

function initializeDB() {
    const db = getDatabase();
    db.transaction(function(tx) {

            console.log("Recreate tables");
            tx.executeSql('CREATE TABLE IF NOT EXISTS ' + getBookProgressTable() + ' (name TEXT UNIQUE, progress NUMERIC)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS ' + getLastBookOpenTable() + ' (id integer primary key autoincrement, name TEXT UNIQUE)');
        }
    )
}

function setLastOpen(bookPath){
    const db = getDatabase();
    db.transaction(function(tx) {
        tx.executeSql('DELETE FROM ' + getLastBookOpenTable());
        tx.executeSql('INSERT INTO ' + getLastBookOpenTable() + ' (name) VALUES (?)', [bookPath]);
    });
}

function getLastOpen() {
    const db = getDatabase();
    let bookPath = "";
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT name FROM ' + getLastBookOpenTable());
        if (rs.rows.length > 0) {
            bookPath = rs.rows.item(0).name;
        }
    });
    console.log("Last open book: " + bookPath);
    return bookPath;
}

function getProgress(bookPath) {
    const db = getDatabase();
    let progress = 0;
    db.transaction(function(tx) {
        var rs = tx.executeSql('SELECT progress FROM ' + getBookProgressTable() + ' WHERE name = ?', [bookPath]);
        if (rs.rows.length > 0) {
            progress = rs.rows.item(0).progress;
        } else {
            tx.executeSql('INSERT INTO ' + getBookProgressTable() + ' (name, progress) VALUES (?, ?)', [bookPath, 0]);
        }
    });
    return progress;
}

function saveProgress(progress) {
    const db = getDatabase();
    db.transaction(function (tx) {
        tx.executeSql('INSERT INTO ' + getBookProgressTable() + '(name, progress) VALUES (?, ?) ON CONFLICT(name) DO UPDATE SET progress = excluded.progress WHERE name = excluded.name', [bookPath, progress]);
    });
}