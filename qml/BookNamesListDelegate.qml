import QtQuick 2.4

import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.1

import Fileutils 1.0

/* import folder */
import "./dialog"
import "js/files.js" as Files

/*
   Delegate component used to manage the imported books.
   The available management operations (open and delete) are shown with a Swipe movement.
*/
ListItem {
    id: standardItem
    height: layout.height + (divider.visible ? divider.height : 0)

    SlotsLayout {
        id: layout
        Image {
            id: coverImage
            asynchronous: true
            fillMode: Image.PreserveAspectFit
            sourceSize.width: units.gu(20)
            height: units.gu(20)
            source: coverArtPath
            SlotsLayout.position: SlotsLayout.Leading
        }
        mainSlot: Label {
            id: label
            verticalAlignment: Text.AlignVCenter
            text: "<b>" + bookName + "</b>"
            height: coverImage.height
            wrapMode: Text.WordWrap
        }
    }
    MouseArea {
        id: selectableMouseArea
        anchors.fill: parent
        onClicked: {
            bookListView.currentIndex = index
            root.bookSelected(bookName)
        }
    }

    /*
       Swipe to right movement: delete book file
       NOTE: is deleted the App local copy (ie: the one under: /home/phablet/.local/share/<project_name>/books/),
       NOT the original one owned by the App source chosen
     */
    leadingActions: ListItemActions {
        actions: [
            Action {
                iconName: "delete"
                onTriggered: {
                    /* book name without path */
                    var bookToDelete = importedBooksListModel.get(index).bookName;
                    var bookPath = Fileutils.getHomePath() + "/" + Files.getBooksSavingRootPath() + "/audiobooks/" + bookToDelete;

                    /* remove book file from the device file system */
                    Fileutils.removeFile(bookPath);

                    /* update the ListModel */
                    importedBooksListModel.remove(index);
                }
            }
        ]
    }
}
