# Audiobook Player

Audiobook player for ubuntu touch using ContentHub with per book position history, thumbnails and sleep timer.

<a href="https://open-store.io/app/audiobookplayer.ledsys"><img src="https://open-store.io/badges/en_US.png" alt="OpenStore" /></a>

## Seeking
There are various controls for seeking in your book:
![controls.png](controls.png)
1. **Seek bar**: You can drag the seek bar to the desired position in the book.
2. **Current position**: The current position in the book is displayed in the format `HH:MM:SS`. Press this to open the `seek to` dialog.
3. **Back to start button**: Press this to seek to the start of the book.
4. **Back 1 minute button**: Press this to seek back 1 minute.
5. **Forward 1 minute button**: Press this to seek forward 1 minute.
6. **Forward to end button**: Press this to seek to the end of the book.

## Position history

The position history remembers the position of the audiobook you are listening to. This is useful if 
you are listening to more than one book or restart your phone. The position history is stored in an sqlite database and
is updated by the app itself. The memory isn't perfect as the app has to be active (phone unlocked and app in the foreground)
to update the position. If you are using bluetooth headphones and pause the playback using the headphones, then turn off your
phone or restart it or close the app without bringing it to the foreground, the position will not be updated and remain
at the last point the app was in the foreground. This is a limitation of the current implementation.

## Sleep Timer

The sleep timer is a feature that allows you to set a timer to stop the playback after a certain amount of time. This is 
useful if you want to listen to an audiobook before going to sleep. Presently the sleep timer is only available when an 
updated version of media-hub is installed from [here](https://gitlab.com/stevenleadbeater/media-hub). Once the 
corresponding merge request completes, the sleep timer will work with the default media-hub.

### Contributing

When contributing to this repository, feel free to first discuss the change you
wish to make via issue. Please note we have
a [code of conduct](https://ubports.com/code-of-conduct), please follow it in
all your interactions with the project.

